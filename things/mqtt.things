//=======================================================================================//
// File: things/mqtt.things  Rev. 09.09.2019 / 09:15                                     //
// (C) 2019 IoT-Systems, D-83043 Bad Aibling                                             //
// Author: Andreas Kriwanek                                                              //
//=======================================================================================//
// Project: Kriwanek's Home                                                              //
//=======================================================================================//
// THIS IS FOR MQTT BINDING 2.4.x!!!                                                     //
//=======================================================================================//
// https://www.openhab.org/addons/bindings/mqtt.generic/

Bridge mqtt:broker:mosquitto "Mosquitto local" [ host="ohabint.iot", secure=false ]
{
	//==================================================================================
	// AUSSSENBEREICH:
	//----------------------------------------------------------------------------------
	// Wetterstation Wemos D1 Mini. Hostname: IOT-WEATHER1
	Thing mqtt:topic:GA-WEATHER1 "Wetterstation 1" {
		Channels:
			Type number : chTemp "Temperatur" [ 
							stateTopic="Home/IOT-WEATHER1/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.BME280.Temperature" ]
			Type number : chHum "Luftfeuchtigkeit" [ 
							stateTopic="Home/IOT-WEATHER1/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.BME280.Humidity" ]
			Type number : chPres "Luftdruck" [ 
							stateTopic="Home/IOT-WEATHER1/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.BME280.Pressure" ]
			Type number : chPresSL "Luftdruck Meereshöhe" [ 
							stateTopic="Home/IOT-WEATHER1/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.BME280.SeaPressure" ]
			Type number : chIlu "Beleuchtungsstärke" [ 
							stateTopic="Home/IOT-WEATHER1/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.TSL2561.Illuminance" ]
	}
	
	//==================================================================================
	// KELLERGESCHOSS:
	//----------------------------------------------------------------------------------
	// HEIZUNG:
	//----------------------------------------------------------------------------------
	// Shelly Flood. Hostname: shellyflood-6949DF - Original Shelly-Firmware
	// https://shelly-api-docs.shelly.cloud/#shelly-flood
	Thing mqtt:topic:HZ_Flood1 "Th-HZ-Flutsensor" {
		Channels:
			Type number : chFlood 	"Flutalarm Heizung" [ 
									stateTopic="shellies/shellyflood-6949DF/flood" ]
			Type number : chTemp 	"Temperatur Flutalarm Heizung" [ 
									stateTopic="shellies/shellyflood-6949DF/temperature" ]
			Type number : chBat 	"Batteriestand Flutalarm Heizung" [ 
									stateTopic="shellies/shellyflood-6949DF/battery" ]
	}
	//----------------------------------------------------------------------------------
	// Heizung Licht/Pumpe. Shelly 2.5 mit Original Firmware. 03.09.2019
	// Hostname: shellyswitch25-68CE59
	// Kommandos benötigen Kleinschreibung: on off
	// https://community.openhab.org/t/shelly1-and-mqtt-2-4-0-binding/63502/4

	Thing mqtt:topic:Th_HZ_LICHTPUMPE "Th-HZ Licht/Pumpe" {
		Channels:
			Type switch : chRelais1 "Relais 1" [ 
							stateTopic="shellies/shellyswitch25-68CE59/relay/0", 
							on="on", off="off", 
							commandTopic="shellies/shellyswitch25-68CE59/relay/0/command", 
							on="on", off="off" ]
			Type switch : chRelais2 "Relais 2" [ 
							stateTopic="shellies/shellyswitch25-68CE59/relay/1", 
							on="on", off="off", 
							commandTopic="shellies/shellyswitch25-68CE59/relay/1/command", 
							on="on", off="off" ]
	} 
	//----------------------------------------------------------------------------------
	
	//==================================================================================
	// ERDGESCHOSS:
	//----------------------------------------------------------------------------------
	// KÜCHE:
	//----------------------------------------------------------------------------------
	// Schranklicht Links KU. Hostname: shellyplug-s-041B8B. Original Shelly-Firmware
	// https://shelly-api-docs.shelly.cloud/#shelly-plug-status
	Thing mqtt:topic:KU_SLLinks "Th-KU-Schranklicht Links" {
		Channels:
			Type string : chRelais "Küche Schranklicht Links" [ 
									stateTopic="shellies/shellyplug-s-041B8B/relay/0" , 
									commandTopic="shellies/shellyplug-s-041B8B/relay/0/command" ]
	}
	//----------------------------------------------------------------------------------
	// Schranklicht Rechts KU. Hostname: shellyplug-s-7A1DCD. Original Shelly-Firmware
	// https://shelly-api-docs.shelly.cloud/#shelly-plug-status
	Thing mqtt:topic:KU_SLRechts "Th-KU-Schranklicht Rechts" {
		Channels:
			Type string : chRelais "Küche Schranklicht Rechts" [ 
									stateTopic="shellies/shellyplug-s-7A1DCD/relay/0" , 
									commandTopic="shellies/shellyplug-s-7A1DCD/relay/0/command" ]
	}
	//----------------------------------------------------------------------------------
	// Wohnzimmer:
	//----------------------------------------------------------------------------------
	// Shelly 1 mit Tasmota. Hostname: IOT-WZ-Wohnzimmerlicht
	Thing mqtt:topic:WZ_WohnzimmerLicht "Th-WZ-Wohnzimmerlicht" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="Home/IOT-WZ-Wohnzimmerlicht/stat/POWER" , 
							commandTopic="Home/IOT-WZ-Wohnzimmerlicht/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	// Sonoff TH mit Tasmota. Hostname: IOT-WZ-COUCHLICHT
	Thing mqtt:topic:WZ_CouchLicht "Th-WZ-Couchlicht" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="Home/IOT-WZ-Couchlicht/stat/POWER" , 
							commandTopic="Home/IOT-WZ-Couchlicht/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	// Kommode Licht. Sonoff S20 mit Tasmota. Hostname: IOT-WZ-KOMMODE
	Thing mqtt:topic:WZ_Kommode "Th-WZ-Kommode" {
		Channels:
			Type switch : chRelais "Wohnzimmer Kommode" [ 
							stateTopic="Home/IOT-WZ-KOMMODE/stat/POWER" , 
							commandTopic="Home/IOT-WZ-KOMMODE/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	// Rolladen WZ Ost. Hostname: shellyswitch25-BA7802. Original Shelly-Firmware
	Thing mqtt:topic:WZ_RollOst "Th-WZ-Rolladen Ost" {
		Channels:
			Type dimmer : chPosition "Rolladen WZ Ost Position" [ 
									stateTopic="shellies/shellyswitch25-BA7802/roller/0/pos" , 
									commandTopic="shellies/shellyswitch25-BA7802/roller/0/command/pos" ]
	}
	//----------------------------------------------------------------------------------
	// Rolladen WZ Süd. Hostname: shellyswitch25-BA6EC3. Original Shelly-Firmware
	Thing mqtt:topic:WZ_RollSued "Th-WZ-Rolladen Sued" {
		Channels:
			Type dimmer : chPosition "Rolladen Position" [ 
									stateTopic="shellies/shellyswitch25-BA6EC3/roller/0/pos" , 
									commandTopic="shellies/shellyswitch25-BA6EC3/roller/0/command/pos" ]
	}
	//----------------------------------------------------------------------------------
	// RFBridge (433MHz). Sonoff RFBridge mit Tasmota. Hostname: IOT-WZ-RFBridge
	Thing mqtt:topic:WZ_RFBridge "Th-WZ-RFBridge" {
		Channels:
			Type string : chRFData "RF Data" [ 
							stateTopic="Home/IOT-WZ-RFBridge/tele/RESULT" , 
							transformationPattern="JSONPATH:$.RfReceived.Data" ]
	}
	//----------------------------------------------------------------------------------
	// Klima. Sonoff SC mit Tasmota. Hostname: IOT-WZ-KLIMA
	Thing mqtt:topic:WZ_Klima "Th-WZ-Klima" {
		Channels:
			Type number : chTemperature "Temperatur" [ 
							stateTopic="Home/IOT-WZ-KLIMA/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.SonoffSC.Temperature" ]
			Type number : chHumidity "Luftfeuchte" [ 
							stateTopic="Home/IOT-WZ-KLIMA/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.SonoffSC.Humidity" ]
			Type number : chLuminosity "Lichtstärke" [ 
							stateTopic="Home/IOT-WZ-KLIMA/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.SonoffSC.Light" ]
			Type number : chNoise "Lautstärke" [ 
							stateTopic="Home/IOT-WZ-KLIMA/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.SonoffSC.Noise" ]
			Type number : chAirQuality "Luftqualität" [ 
							stateTopic="Home/IOT-WZ-KLIMA/tele/SENSOR" , 
							transformationPattern="JSONPATH:$.SonoffSC.AirQuality" ]
	}
	//----------------------------------------------------------------------------------
	// Esszimmer LICHT
	
	//----------------------------------------------------------------------------------
	// Esszimmer Rolladen. Hostname: shellyswitch25-BA8C3D. Original Shelly-Firmware
	Thing mqtt:topic:EZ_RollNord "Th-EZ-Rolladen Nord" {
		Channels:
			Type dimmer : chPosition	   "Rolladen Dimmer" 	[ 
							stateTopic="shellies/shellyswitch25-BA8C3D/roller/0/pos" , 
							commandTopic="shellies/shellyswitch25-BA8C3D/roller/0/command/pos" ]
	}
	//----------------------------------------------------------------------------------
	// Flur Licht
	
	//----------------------------------------------------------------------------------
	// WC Licht
	
	//----------------------------------------------------------------------------------
	
	//==================================================================================
	// OBERGESCHOSS:
	//----------------------------------------------------------------------------------
	// Flur Licht. Shelly 1 mit Tasmota. Hostname: IOT-OGF-LICHT
	Thing mqtt:topic:OGF_Licht "Th-OGF-Licht" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="HOME/IOT-OGF-LICHT/stat/POWER" , 
							commandTopic="HOME/IOT-OGF-LICHT/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	// Bastelzimmer Licht. Shelly 1 mit Tasmota. Hostname: IOT-BZ-Licht
	Thing mqtt:topic:BZ_Licht "Th-BZ-Licht" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="IOT-BZ-Licht/stat/POWER" , 
							commandTopic="IOT-BZ-Licht/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	// Bastelzimmer Scheinwerfer. Shelly 1PM mit Tasmota. Hostname: IOT-BZ-Fluter
	Thing mqtt:topic:BZ_Fluter "Th-BZ-Fluter" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="IOT-BZ-Fluter/stat/POWER" , 
							commandTopic="IOT-BZ-Fluter/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	// Gästezimmer Licht. Shelly 1 mit Tasmota. Hostname: IOT-GZ-Licht
	Thing mqtt:topic:GZ_Licht "Th-GZ-Licht" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="IOT-GZ-Licht/stat/POWER" , 
							commandTopic="IOT-GZ-Licht/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	// Bad Licht. Shelly 1 mit Tasmota. Hostname: IOT-OGB-Licht
	Thing mqtt:topic:OGB_Licht "Th-OGB-Licht" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="IOT-OGB-Licht/stat/POWER" , 
							commandTopic="IOT-OGB-Licht/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	// Bad Schranklicht. Shelly 1 mit Tasmota. Hostname  IOT-OGB-SCHRANKLICHT
	Thing mqtt:topic:OGB_Schranklicht "Th-OGB-Schranklicht" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="HOME/IOT-OGB-SCHRANKLICHT/stat/POWER" , 
							commandTopic="HOME/IOT-OGB-SCHRANKLICHT/cmnd/POWER" ]
	}
	//----------------------------------------------------------------------------------
	
	
	//==================================================================================
	// DACHGESCHOSS:
	//----------------------------------------------------------------------------------
	// Bad Licht
	//----------------------------------------------------------------------------------
	// Bad Schranklicht
	//----------------------------------------------------------------------------------
	// Ankleide Licht
	//----------------------------------------------------------------------------------
	// Schlafzimmer Licht
	//----------------------------------------------------------------------------------
	// Schlafzimmer LED
	//----------------------------------------------------------------------------------
	// Flur Licht
	//----------------------------------------------------------------------------------
	
	//==================================================================================
	// AUSSENBEREICH:
	//----------------------------------------------------------------------------------
	// Gartenbrunnen/Gartenlicht. Shelly 2 mit Tasmota. Hostname: IOT-GA-BRUNNEN-LICHT
	Thing mqtt:topic:GA_Brunnen-Licht "Th-GA-Brunnen-Licht" {
		Channels:
			Type switch : chRelais1 "Relais1" [ 
							stateTopic="Home/IOT-GA-BRUNNEN-LICHT/stat/POWER1" , 
							commandTopic="Home/IOT-GA-BRUNNEN-LICHT/cmnd/POWER1" ]
			Type switch : chRelais2 "Relais2" [ 
							stateTopic="Home/IOT-GA-BRUNNEN-LICHT/stat/POWER2" , 
							commandTopic="Home/IOT-GA-BRUNNEN-LICHT/cmnd/POWER2" ]
	}
	//----------------------------------------------------------------------------------
	// Terrasse WS2813. Wemos D1 mit Tasmota. Hostname IOT-TS-WS2813
	//----------------------------------------------------------------------------------
	Thing mqtt:topic:TS_WS2813 "Th-TS-WS2813 LED-Stripe" {
		Channels:
			Type switch : chRelais "Relais" [ 
							stateTopic="Home/IOT-TS-WS2813/stat/POWER" , 
							commandTopic="Home/IOT-TS-WS2813/cmnd/POWER" ]
			Type dimmer : chDimmer "Dimmer" [ 
							stateTopic="Home/IOT-TS-WS2813/stat/Dimmer" , 
							commandTopic="Home/IOT-TS-WS2813/cmnd/Dimmer" ]
			Type number : chScheme "Schema" [ 
							stateTopic="Home/IOT-TS-WS2813/stat/Scheme" ,
							commandTopic="Home/IOT-TS-WS2813/cmnd/Scheme" ]
			Type colorHSB  : chColorWheel "Farbrad" [
							stateTopic="Home/IOT-TS-WS2813/stat/HSBColor" , 
							commandTopic="Home/IOT-TS-WS2813/cmnd/HSBColor" ]						
			Type number : chFixcolor "Fix Colors" [ 
							stateTopic="Home/IOT-TS-WS2813/stat/Color" , 
							commandTopic="Home/IOT-TS-WS2813/cmnd/Color" ]
			Type number : chSpeed "Speed" [ 
							stateTopic="Home/IOT-TS-WS2813/stat/Speed" , 
							commandTopic="Home/IOT-TS-WS2813/cmnd/Speed" ]
			Type switch : chFade "Fade" [ 
							stateTopic="Home/IOT-TS-WS2813/stat/Fade" , 
							commandTopic="Home/IOT-TS-WS2813/cmnd/Fade" ]
	}
	//----------------------------------------------------------------------------------
	// Terasse Licht/Sicherheit Fluter. Shelly 2.5 mit Original Firmware. 
	// Hostname: shellyswitch25-BA6FA7
	// Kommandos benötigen Kleinschreibung: on off
	Thing mqtt:topic:TS_LICHT "Th-TS-LICHT" {
		Channels:
			Type string : chRelais1 "Relais 1" [ 
							stateTopic="shellies/shellyswitch25-BA6FA7/relay/0" , 
							commandTopic="shellies/shellyswitch25-BA6FA7/relay/0/command" ]
			Type string : chRelais2 "Relais 2" [ 
							stateTopic="shellies/shellyswitch25-BA6FA7/relay/1" , 
							commandTopic="shellies/shellyswitch25-BA6FA7/relay/1/command" ]
	} 
}
 