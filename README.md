# OpenHAB2 Configuration Kriwanek #

This repository contains all files for configuring a running OpenHAB2 installation for my home automation. It is based mainly on the MQTT binding.

### What is this repository for? ###

* You can copy the different configurations files to your own OpenHAB device as a template
* Actually the repository is version 1.00.00

### File locations ###

If you have installed with the OpenHabian image, then you will find the configuration directories and files here:

/etc/openhab2:

* html
* icons
* items
* persistence
* rules
* scripts
* services
* sitemaps
* sounds
* things
* transform

You can mount the directory in Windows as "openHAB-conf" share.

### How do I get set up? ###

* Install your Raspberry Pi (Model 3 or 4) with the Raspbian distribution
* Install Mosquitto on the Raspberry Pi
* Install the NTP and MQTT binding via PaperUI
* Copy files from this repository to your Raspberry Pi directories and edit it
* Restart OpenHAB
* Use it.

### Contribution guidelines ###

* You can use the repository for free - no fees!
* Send me messages with tipps or error corrections

### Who do I talk to? ###

* Users with OpenHAB2 home automation